###MLD 
### Informations :
- Une clé artficielle a été ajouté pour ces relations : 
- Employé : j'ai ajouté ID, j'ai fais le choix d'ajouter cette clé car elle je n'ai pas voulu choisir la clé {nom,prénom} ou adresse_mail qui est trop long.
- Appel_projet : j'ai ajouté Id_appel_projet
- Proposition_projet : j'ai ajouté Id_proposition_projet
- Budget : j'ai ajouté Id_ligne_budget
- Projet : j'ai ajouté Id_projet
- Dépense : j'ai ajouté Id_depense
- Toutes les tables sont en 3NF


### Choix des héritages :

#### Héritage 1 : Entite_juridique – Financeur, Laboratoire :

J’ai choisi un héritage par référence car la classe mère n’est pas abstraite, et je n’ai pas de contraintes complexes systématique car j’ai un héritage non exclusif (un laboratoire peut être financeur).

* **Entite_juridique** (#code, type_entité, date_debut_activite, date_fin_activite)
* **Financeur** (#code=> Entite_juridique, id_contact=>Contact)
* **Organisme_projet**(#code=> Entite_juridique,nom)
* **Laboratoire** (#code=> Entite_juridique) 
    - Contraintes : 
    - Restriction (Projection(Entite_juridique, code), type_entité="Laboratoire")= Projection(Laboratoire, code)
    - Restriction (Projection(Entite_juridique, code), type_entité="Organisme_projet")= Projection(Organisme_projet, code)
    - Vues : 
    - **vFinanceur**=jointure(Entite_juridique, Financeur, code = code)
    - **vLaboratoire**=jointure(Laboratoire, Entite_juridique, code = code)
    - **vOrganisme_projet**=jointure(Laboratoire, Entite_juridique, code = code)



#### Héritage 2 : Employe – Contact, Membre_du_comité_de_personne, Membre_du_laboratoire  :

J’ai choisi un héritage par classe fille car la classe mère est abstraite et ne possède pas des associations avec d’autres classes.

* **Contact**(#Id_contact, titre, mail, nom ,telephone)
* **Membre_comite_personne**(#Id_membre_comite,nom)
* **Membre_du_laboratoire**(#Id_membre_du_laboratoire, nom, fonction, mail)
    -  Vues : 
    -  **vEmploye**=Union(Projection(Membre_du_laboratoire,Id_membre_du_laboratoire,nom),Projection(Contact,Id_contact,nom),Projection(Membre_comite_personne,Id_membre_comite,nom))

### Héritage 3 : Membre_du_laboratoire - Enseignant_chercheur, Ingenieur_de_recherche,  Doctorant :

J’ai choisi héritage par classe mère car la classe mère est associé à plusieurs autres classes, et j’ai un héritage presque complet donc je vais devoir exprimer que des contraintes simples.

* **Membre_du_laboratoire**(Id_membre_labo, nom, adresse_mail, Qte_temps_recherche, etablissement_associe, domaine_specialite, date_debut_these, date_fin_these, sujet_these, fonction, type {enseignant_chercheur, ingenieur_de_recherche, doctorant}})

*  Contraintes simples:
    - Type NOT NULL
    - Type = Enseignant_chercheur AND Qte_temps_recherche NOT NULL AND etablissement_associe NOT NULL AND  domaine_specialite NULL AND date_debut_these NULL AND date_fin_these NULL AND sujet_these NULL
    - Type = Ingenieur_de_recherche AND Qte_temps_recherche NULL AND etablissement_associe NULL AND  domaine_specialite NOT NULL AND date_debut_these NULL AND date_fin_these NULL AND sujet_these NULL
    - Type = Doctorant AND Qte_temps_recherche NULL AND etablissement_associe NULL AND  domaine_specialite NULL AND date_debut_these NOT NULL AND date_fin_these NOT NULL AND sujet_these NOT NULL
    - Vues : 
    - **vEnseignant_chercheur**=projection(restriction(Membre_du_laboratoire,Type=Enseignant_chercheur),Qte_temps_recherche,etablissement_associe)
    - **vIngenieur_de_recherche**=projection(restriction(Membre_du_laboratoire,Type=Ingenieur_de_recherche),domaine_specialite)
    - **vDoctorant**=projection(restriction(Membre_du_laboratoire,Type=Doctorant),date_debut_these,sujet_these,date_fin_these)

### Association N : M

* Entite_juridique – Label

    - **Labels_des_entites** (#code=> Entite_juridique , #id_label => Label)
    - Contraintes : PROJECTION(Entite_juridique,code) = PROJECTION(Labels_des_entites,code) (association 1..* coté Entite_juridique)
    
* Label – Proposition_projet

    - **Labels_des_propositions_projets**(#id_proposition_projet=>Proposition_projet , #id_label=>Label)

* Financeur – Organisme_projet

    - **Organisme_projet_des_financeurs**(#code=> Financeur, #code=> Organisme_projet)
    - Contraintes : PROJECTION(Financeur,code) = PROJECTION(Organisme_projet_des_financeurs,code)

* Membre_comite_personne - Organisme_projet

    - **Membre_comite_personne_de_organisme_projet**(#Id_membre_comite=>Membre_comite_personne, #code=> Organisme_projet)
    - Contraintes : PROJECTION(Membre_comite_personne_de_organisme_projet,Id_membre_comite) = PROJECTION(Membre_comite_personne,Id_membre_comite) AND PROJECTION(Organisme_projet,code) = PROJECTION(Membre_comite_personne,code)
    
* Membre_comite_personne - Appel_projet

    - **Gerants_des_appels_projets**(#Id_appel_projet=> Appel_projet, #Id_membre_comite=>Membre_comite_personne)
    - Contraintes : PROJECTION(Gerants_des_appels_projets,Id_membre_comite) = PROJECTION(Membre_comite_personne,Id_membre_comite)
    
* Membre_comite_personne - Proposition_projet

    - **Repondeurs_des_propositions_projets**(#Id_proposition_projet=> Proposition_projet, #Id_membre_comite=>Membre_comite_personne)

* Membre_du_laboratoire - Proposition_projet

    - **Redigeant_des_propositions_projet**(#Id_membre_du_laboratoire=> Membre_du_laboratoire, #Id_proposition_projet=> Proposition_projet)
    - Contraintes : PROJECTION(Membre_du_laboratoire,Id_membre_du_laboratoire) = PROJECTION(Proposition_projet,Id_membre_du_laboratoire)
    
* Membre_du_laboratoire - Projet

    - **Participant_du_projet**(#Id_membre_du_laboratoire=> Membre_du_laboratoire, #Id_projet=> Projet)



### Association 1 : N

* Contact – Financeur 

    - **Financeur**(#code=> Entite_juridique, Id_contact=>Contact)
    - Contraintes : Financeur.Id_contact not null and PROJECTION(Contact,Id_contact) = PROJECTION(Financeur,Id_contact)
    
* Membre_du_laboratoire - Laboratoire
    - **Membre_du_laboratoire**(Id_membre_labo, nom, adresse_mail, Qte_temps_recherche, etablissement_associe, domaine_specialite, date_debut_these, date_fin_these, sujet_these, fonction, type {enseignant_chercheur, ingenieur_de_recherche, doctorant},laboratoire=>Laboratoire)
    - Contraintes : Membre_du_laboratoire.laboratoire not null and PROJECTION(Membre_du_laboratoire,laboratoire) = PROJECTION(Laboratoire,code)
    - 
* Appel_projet – Organisme_projet
    - **Appel_projet**(#Id_appel_projet, date_de_lancage, duree, theme, description, code=> Organisme_projet) 
    - Contraintes : Appel_projet.code not null

* Proposition_projet - Appel_projet
    - **Proposition_projet**(#Id_proposition_projet, objet, type_financement, date_de_depot, date_de_reponse, id_appel_projet => Appel_projet)
    - Contraintes : Proposition_projet.id_appel_projet not null

* Proposition_projet - Laboratoire
    - **Proposition_projet**(#Id_proposition_projet, objet, type_financement, date_de_depot, date_de_reponse, id_appel_projet => Appel_projet, code=> Laboratoire)
    - Contraintes : Proposition_projet.Code not null

* Depense – Membre_du_laboratoire
    - **Depense**(#Id_depense ,type_de_financement, montant, date_de_depense, validateur=> Membre_du_laboratoire, demandeur=> Membre_du_laboratoire)
    - Contraintes : Depense.demandeur not null, Depense.validateur not null

* Projet – Budget 
    - **Budget**(#Id_ligne_budget,montant, id_projet=>Projet, objet,type_financement {fonctionnement, materiel}) 
    - Contraintes : Budget.id_projet not null and PROJECTION(Budget,id_projet) = PROJECTION(Projet,id_projet) et type_financement NOT NULL et Objet NOT NULL

### Composition :

* Proposition_projet – Budget 
    - **Budget**(#Id_ligne_budget, #id_proposition_projet=> Proposition_projet,montant,  id_projet=>Projet, objet,type_financement {fonctionnement, materiel})
    - Contraintes : PROJECTION(Budget,id_proposition_projet) = PROJECTION(Proposition_projet,id_proposition_projet) et type_financement NOT NULL et Objet NOT NULL

* Projet – Depense 
    - **Depense**(#Id_depense ,type_de_financement, montant, date_de_depense, validateur=> Membre_du_laboratoire, demandeur=> Membre_du_laboratoire, #id_projet=> Projet)


### Association 1 : 1

* Proposition_projet - Projet
*   - **Projet**(#Id_projet, date_de_debut, date_de_fin, id_proposition_projet=> Proposition_projet)
    - Contraintes : id_proposition_projet unique

### Autres contraintes :
*  Les membres du comite de personnes ne peuvenet gérer que les appels projets de leurs organismes projets.
*  Les membre du comite comite de personnes ne repondent qu'aux propositions de projets qui concernent les appels projets de leurs organsimes projet.
*  Les membres de laboratoire redige les propositions de projets liées à leurs laboratoire
*  La somme des depenses d'un projet doit etre inférieur à la somme du budget du projet'

   





