# Note de Clarification
### Contexte

Un laboratoire de recherche veut créer un logiciel de base de données pour tenir à jour l'ensemble de ses projets de recherche. Ce logiciel va lui permettre de répondre aux appels d'offres crées par des financeurs et ensuite gérer le projet en gérant ses employés, les dépenses et pouvoir faire des statistiques.

### Liste des objets qui devront être gérés dans la base de données
*   Financeur
*   Contact
*   Organisme_projet
*   Appel_projet
*   Employé d'organisme
*   Proposition_projet
*   Laboratoire
*   Label
*   Budget
*   Dépense
*   Projet
*   Employé_laboratoire
*   Enseignant_chercheur
*   Ingenieur_chercheur
*   Enseignant_chercheur
*   Doctorant

***

### Liste des propriétés associées à chaque objet


#### Financeur
- Type d'entité
- Date_début_activité
- Date_fin_activité


#### Contact
-  Titre
-  Mail 
-  Téléphone


#### Organisme_projet
- Date_de_création
- Durée
- Nom
- Valide


##### Appel_projet
- Date_de_lancage
- Durée
- Thème
- Description

##### Gérant_appel_projet
- Adresse_mail
- nom
- prénom
- téléphone

##### Proposition_projet
- Objet
- Type_de_financement
- Date_de_dépot
- Date_de_réponse
- Acceptation

#### Budget
- Montant

#### Depense
- Montant_global
- Type_de_depense

#### Projet
- Date_de_début
- Date_de_fin 

#### Ingénieur_de_recherche
- Domaine_spécialité

#### Enseignant_chercheur
- Qte_temps_recherche
- Etablissement associé

#### Doctorant
- Date_debut_thèse
- Date_fin_thèse
- Sujet_thèse

### Liste des associations : 

##### Les associations N : M  :

- Une entité juridique peut donner plusieurs labels et un label peut être donné par plusieurs entités juridiques : N : M
- Un label peut être donné à plusieurs propositions de projet et une proposition de projet peut avoir plusieurs labels. N : M
- Un financeur peut créer plusieurs organismes projets et un organisme projet est créé par un ou plusieurs financeurs. N : M
- Une personne faisant partie du comité de personnes (qui gère les appels projets) travaille dans un ou plusieurs organismes projets et un organisme projet est associé à plusieurs personnes. N : M
- Un comité de personne gère plusieurs appels projet et un appel projet est géré par un ou plusieurs personnes. N : M
- Une personne faisant partie du comité de personnes (qui gère les appels projets) répond à plusieurs propositions de projet et une proposition de projet reçoit une réponse par un ou plusieurs personnes. N :M 
- Les membres du projet peuvent créer un ou plusieurs projets et un projet est créé par plusieurs membres du projet.  N : M
- Une proposition de projet est rédigée par un ou plusieurs membres du laboratoire et un membre de laboratoire peut rédiger plusieurs propositions de projets. N : M

##### Les associations 1 : N  :

- Un financeur possède un contact et un contact peut travailler avec plusieurs financeurs. 1 : N
- Un organisme projet publie plusieurs appels projet et un appel projet est publié par un seul organisme projet 1 : N
- Un laboratoire peut créer plusieurs propositions de projets et une proposition de projet est créée par un seul laboratoire. 1 : N
- Une proposition de projet peut répondre à un seul appel projet et un appel projet peut avoir plusieurs propositions de projet. 1 : N
- Une proposition de projet possède un ou plusieurs budgets. (Je considère qu'on ne peut pas avoir de proposition de projet sans budget) et un budget est associé à une seule proposition de projet. 1 : N 
- Un membre de laboratoire travaille dans un seul laboratoire et un laboratoire peut avoir plusieurs membres de laboratoire. 1 : N
- Un projet possède un ou plusieurs budgets et un budget est associé à un seul projet. 1 : N
- Un projet possède plusieurs dépenses et une dépense est associée à un seul projet. 1 : N

#### Les associations 1 : 1 :

- Une proposition de projet peut correspondre à un projet et un projet correspond à une seule proposition de projet. 1 : 1


### Liste des utilisateurs (rôles) appelés à modifier et consulter les données
##### Les membres du laboratoire
##### Les financeurs
##### Les membres des organismes projets

***

### Liste des fonctions que ces utilisateurs pourront effectuer
##### Les membres du laboratoire 
- il pourra rentrer l'ensemble de données liées au laboratoire, aux propositions de projets, aux employés du laboratoire, aux participants du projet, aux dépenses et aux budgets du projet. Cela lui permettra d'avoir une vue d'ensemble sur son laboratoire lui facilitant la prise de décision afin de mieux gérer ces projets.
- Il peut *consulter* l'ensemble des vues liées au laboratoire, aux propositions de projets, aux employés du laboratoire, aux participants du projet aux dépenses et aux budgets du projet.

##### Les financeurs et les membres des organismes de projets
- Il pourra gérer l'ensemble de données lieées aux organismes projets, aux appels à projets,  aux financeurs, et aux employés des organismes projets.
- Il peut *consulter* l'ensemble des vues liées aux organismes projets, aux appels à projets,  aux financeurs, et aux employés des organismes projets.

***

### Liste des vues (je vais en ajouter d'autres)
- les projets pour lesquels il reste du budget à dépenser.
- les projets en cours.
- les appels d'offre en cours sans réponse.
- Les membres du projet externes et internes.
- Les employés qui ont lancé les appels projets.
- Les validateurs et les demandeurs des dépenses.
- Les propositions de projets faites par un laboratoire.
- Les membres du laboratoire qui ont fait la proposition de projet.
- Les labels donnés aux proposition de projets.
- Les dépenses liées à un projet en précisant le demandeur et le validateur.
- Nombre de propositions répondues par type d'organisme projet avec le nombre de projet accepté.
- Membres du laboratoire affectés au plus grand nombre de projet en cours.
- Mois de l'année où on fait le plus de dépenses de projet au laboratoire

