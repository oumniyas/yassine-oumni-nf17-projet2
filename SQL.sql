DROP TABLE IF EXISTS Entite_juridique CASCADE;
DROP TABLE IF EXISTS Organisme_projet CASCADE;
DROP TABLE IF EXISTS Financeur CASCADE;
DROP TABLE IF EXISTS Laboratoire CASCADE;
DROP TABLE IF EXISTS Contact CASCADE;
DROP TABLE IF EXISTS Membre_du_laboratoire CASCADE;
DROP TABLE IF EXISTS Membre_comite_personne CASCADE;
DROP TABLE IF EXISTS Appel_projet CASCADE;
DROP TABLE IF EXISTS Projet CASCADE;
DROP TABLE IF EXISTS Proposition_projet CASCADE;
DROP TABLE IF EXISTS Budget CASCADE;
DROP TABLE IF EXISTS Label CASCADE;
DROP TABLE IF EXISTS Labels_des_propositions_projets CASCADE;
DROP TABLE IF EXISTS Membre_comite_personne_des_organismes_projet CASCADE;
DROP TABLE IF EXISTS Gerants_des_appels_projets CASCADE;
DROP TABLE IF EXISTS Repondeurs_des_propositions_projets CASCADE;
DROP TABLE IF EXISTS Redigeant_des_propositions_projet CASCADE;
DROP TABLE IF EXISTS Depense CASCADE;
DROP TABLE IF EXISTS Participant_du_projet CASCADE;
DROP TABLE IF EXISTS organisme_projet_des_financeurs CASCADE;

CREATE TABLE Entite_juridique(
    Code VARCHAR PRIMARY KEY,
    type_entite VARCHAR NOT NULL,
	Nom VARCHAR NOT NULL,
    date_debut_activite DATE NOT NULL, 
    date_fin_activite DATE
);

CREATE TABLE Organisme_projet(
   Code VARCHAR NOT NULL REFERENCES Entite_juridique(Code),
   PRIMARY KEY (Code)
    
    
);

CREATE TABLE Contact(
    Id_contact INTEGER UNIQUE,
    Titre VARCHAR NOT NULL,
    Nom VARCHAR NOT NULL,
    Email VARCHAR NOT NULL UNIQUE  ,
    Telephone VARCHAR NOT NULL UNIQUE ,
    PRIMARY KEY (Id_contact,Email,Telephone)
);
CREATE TABLE Financeur(
    Code VARCHAR NOT NULL REFERENCES Entite_juridique(Code),
	Id_contact INTEGER NOT NULL REFERENCES Contact(Id_contact),
    PRIMARY KEY (Code)
    
);
CREATE TABLE Laboratoire(
    Code VARCHAR NOT NULL REFERENCES Entite_juridique(Code),
    PRIMARY KEY (Code)
    
);




CREATE TABLE Membre_du_laboratoire(
    id_membre_du_laboratoire INTEGER unique,
    Nom VARCHAR NOT NULL,
    Email VARCHAR NOT NULL unique,
    type VARCHAR CHECK (Type IN ('Enseignant_chercheur', 'Ingenieur_de_recherche', 'Doctorant')) NOT NULL,
    Qte_temps_recherche INTEGER, 
    etablissement_associe VARCHAR, 
    domaine_specialite VARCHAR, 
    date_debut_these DATE, 
    date_fin_these DATE, 
    sujet_these VARCHAR,
    CHECK ((type = 'Enseignant_chercheur' AND Qte_temps_recherche IS NOT NULL AND etablissement_associe IS NOT NULL AND  domaine_specialite IS NULL AND date_debut_these IS NULL AND date_fin_these IS NULL AND sujet_these IS NULL) 
    OR (type = 'Ingenieur_de_recherche' AND Qte_temps_recherche IS NULL AND etablissement_associe IS NULL AND  domaine_specialite IS NOT NULL AND date_debut_these IS NULL AND date_fin_these IS NULL AND sujet_these IS NULL)
    OR (type = 'Doctorant' AND Qte_temps_recherche IS NULL AND etablissement_associe IS NULL AND  domaine_specialite IS NULL AND date_debut_these IS NOT NULL AND date_fin_these IS NOT NULL AND sujet_these IS NOT NULL)
    ),
	Laboratoire varchar not null references Laboratoire(code),
	PRIMARY KEY(id_membre_du_laboratoire,Email)
);

CREATE TABLE Membre_comite_personne(
    id_membre_comite INTEGER PRIMARY KEY,
    Nom VARCHAR NOT NULL
);



CREATE TABLE Appel_projet(
    id_appel_projet INTEGER PRIMARY KEY, 
    Date_de_lancage DATE NOT NULL, 
    Date_de_fin DATE NOT NULL, 
    Theme VARCHAR NOT NULL, 
    Description VARCHAR NOT NULL, 
    organisme_projet VARCHAR NOT NULL REFERENCES Organisme_projet(Code) 
);

CREATE TABLE Proposition_projet(
    Id_appel_projet INTEGER NOT NULL  REFERENCES Appel_projet(id_appel_projet), 
    id_proposition_projet INTEGER NOT NULL UNIQUE, 
    date_de_depot DATE NOT NULL, 
    date_de_reponse DATE, 
    Reponse VARCHAR CHECK (Reponse IN ('refusée', 'acceptée')),
    code VARCHAR NOT NULL REFERENCES Laboratoire(Code),
    PRIMARY KEY (Id_proposition_projet) 
);

CREATE TABLE Projet(
    id_projet INTEGER PRIMARY KEY, 
    Date_de_debut DATE NOT NULL,
    Date_de_fin DATE NOT NULL,
    id_proposition_projet INTEGER NOT NULL REFERENCES Proposition_projet(Id_proposition_projet),
	Depense json not null
);




CREATE TABLE Budget(
    Id_ligne_budget INTEGER, 
    Id_proposition_projet INTEGER NOT NULL REFERENCES Proposition_projet(Id_proposition_projet), 
    Id_projet INTEGER NULL REFERENCES Projet(id_projet),
    Montant INTEGER NOT NULL, 
    Objet VARCHAR NOT NULL, 
    Type_de_financement VARCHAR CHECK (Type_de_financement IN ('fonctionnement', 'matériel')) NOT NULL,
    PRIMARY KEY (Id_ligne_budget,Id_proposition_projet) 
);

CREATE TABLE Label( 
    Titre VARCHAR PRIMARY KEY
   
);

CREATE TABLE Labels_des_propositions_projets( 
    Id_proposition_projet INTEGER NOT NULL REFERENCES Proposition_projet(Id_proposition_projet), 
    Titre VARCHAR NOT NULL REFERENCES Label(Titre),
    PRIMARY KEY (Id_proposition_projet,Titre) 
);
CREATE TABLE Organisme_projet_des_financeurs( 
    code VARCHAR NOT NULL REFERENCES Organisme_projet(Code), 
    Financeur VARCHAR NOT NULL REFERENCES Financeur(Code),
    PRIMARY KEY (code,Financeur) 
);

CREATE TABLE Membre_comite_personne_des_organismes_projet( 
    id_membre_comite INTEGER NOT NULL REFERENCES Membre_comite_personne(id_membre_comite),
    organisme_projet varchar NOT NULL REFERENCES Organisme_projet(Code),
    PRIMARY KEY (id_membre_comite,organisme_projet) 
);

CREATE TABLE Gerants_des_appels_projets( 
    id_appel_projet INTEGER NOT NULL REFERENCES Appel_projet(id_appel_projet),
    id_membre_comite INTEGER NOT NULL REFERENCES Membre_comite_personne(id_membre_comite),
    PRIMARY KEY (id_membre_comite,id_appel_projet) 
);

CREATE TABLE Repondeurs_des_propositions_projets( 
    id_proposition_projet INTEGER NOT NULL REFERENCES Proposition_projet(Id_proposition_projet),
    id_membre_comite INTEGER NOT NULL REFERENCES Membre_comite_personne(id_membre_comite),
    PRIMARY KEY (id_membre_comite,id_proposition_projet) 
);

CREATE TABLE Redigeant_des_propositions_projet( 
    id_membre_du_laboratoire INTEGER NOT NULL REFERENCES Membre_du_laboratoire(id_membre_du_laboratoire),
    id_proposition_projet INTEGER NOT NULL REFERENCES Proposition_projet(Id_proposition_projet),
    PRIMARY KEY (id_membre_du_laboratoire,id_proposition_projet) 
);

/*CREATE TABLE Depense( 
    Id_depense INTEGER PRIMARY KEY, 
    Id_Projet INTEGER NOT NULL REFERENCES Projet(id_projet), 
    Demandeur INTEGER NOT NULL REFERENCES Membre_du_laboratoire(id_membre_du_laboratoire),  
    Validateur INTEGER NOT NULL REFERENCES Membre_du_laboratoire(id_membre_du_laboratoire), 
    Date_de_depense DATE NOT NULL, 
    Montant INTEGER NOT NULL, 
   Type_de_financement VARCHAR CHECK (Type_de_financement IN ('fonctionnement', 'matériel')) NOT NULL
);*/

CREATE TABLE Participant_du_projet( 
    Id_projet INTEGER NOT NULL REFERENCES Projet(id_projet), 
    Id_membre_du_laboratoire INTEGER NOT NULL REFERENCES Membre_du_laboratoire(Id_membre_du_laboratoire),  
    Fonction VARCHAR NOT NULL,
    PRIMARY KEY (Id_projet,Id_membre_du_laboratoire) 
);

INSERT INTO Entite_juridique (Code,type_entite, Nom, date_debut_activite) VALUES ('FR','Pays','France', '1792-09-22');
INSERT INTO Entite_juridique (Code,type_entite, Nom, date_debut_activite) VALUES ('FRE', 'Entreprise','Free', '1988-04-12');
INSERT INTO Entite_juridique (Code,type_entite, Nom, date_debut_activite) VALUES ('HF','Region' ,'Haut de France', '2014-03-18');
INSERT INTO Entite_juridique (Code,type_entite, Nom, date_debut_activite) VALUES ('P', 'Ville','Paris', '1996-01-30');
INSERT INTO Entite_juridique (Code,type_entite, Nom, date_debut_activite) VALUES ('LA', 'Laboratoire','Lab_paris', '1996-01-30');
INSERT INTO Entite_juridique (Code,type_entite, Nom, date_debut_activite) VALUES ('LB', 'Laboratoire','Lab_compiene', '1996-01-30');
INSERT INTO Entite_juridique (Code,type_entite, Nom, date_debut_activite) VALUES ('C', 'Ville','Compiegne', '1996-01-30');
INSERT INTO Entite_juridique (Code,type_entite, Nom, date_debut_activite) VALUES ('O', 'Organisme','Organisme1', '1996-01-30');
INSERT INTO Entite_juridique (Code,type_entite, Nom, date_debut_activite) VALUES ('O1', 'Organisme','Organisme2', '1996-01-30');

INSERT INTO Organisme_projet (Code) VALUES ('O');
INSERT INTO Organisme_projet (Code) VALUES ('O1');

INSERT INTO Laboratoire (Code) VALUES ('LA');
INSERT INTO Laboratoire (Code) VALUES ('LB');

INSERT INTO Contact (Id_contact,Titre,Nom,Email,Telephone) VALUES (1,'Titre 1','Nom 1','contact1@gmail.com','xxxxxxx1');
INSERT INTO Contact (Id_contact,Titre,Nom,Email,Telephone) VALUES (2, 'Titre 2','Nom 2','contact2@gmail.com','xxxxxxx2');
INSERT INTO Contact (Id_contact,Titre,Nom,Email,Telephone) VALUES (3,'Titre 3','Nom 3','contact3@gmail.com','xxxxxxx3');

INSERT INTO Financeur (Code,Id_contact) VALUES ('FR','1');
INSERT INTO Financeur (Code,Id_contact) VALUES ('FRE','2');
INSERT INTO Financeur (Code,Id_contact) VALUES ('C','3');

INSERT INTO Membre_du_laboratoire (id_membre_du_laboratoire,Nom,Email ,type, Qte_temps_recherche, etablissement_associe,laboratoire) VALUES ('1','Nom 1','contact1@gmail.com', 'Enseignant_chercheur', '56', 'UTC','LB');
INSERT INTO Membre_du_laboratoire (id_membre_du_laboratoire,Nom,Email, type, domaine_specialite,laboratoire) VALUES ('2', 'Nom 2','contact2@gmail.com','Ingenieur_de_recherche', 'Biology','LA');
INSERT INTO Membre_du_laboratoire (id_membre_du_laboratoire,Nom,Email, type, date_debut_these, date_fin_these, sujet_these,laboratoire) VALUES ('3', 'Nom 3','contact3@gmail.com','Doctorant', '2015-02-06', '2019-09-29', 'Theorie de graphe','LB');
INSERT INTO Membre_du_laboratoire (id_membre_du_laboratoire,Nom,Email, type, date_debut_these,date_fin_these ,sujet_these,laboratoire) VALUES ('4', 'Nom 4','contact4@gmail.com','Doctorant', '2018-12-08', '2019-09-29','Database design','LA');
INSERT INTO Membre_du_laboratoire (id_membre_du_laboratoire,Nom,Email, type, date_debut_these,date_fin_these ,sujet_these,laboratoire) 
VALUES ('6', 'Nom 2','contact6@gmail.com','Doctorant', '2013-12-08', '2020-09-29','Database design','LA');

INSERT INTO Membre_comite_personne (id_membre_comite,Nom) VALUES ('1', 'Nom1');
INSERT INTO Membre_comite_personne (id_membre_comite,Nom) VALUES ('2', 'Nom2');
INSERT INTO Membre_comite_personne (id_membre_comite,Nom) VALUES ('3', 'Nom3');

INSERT INTO Appel_projet (id_appel_projet, Date_de_lancage, Date_de_fin, Theme, Description, organisme_projet) VALUES ('1', '2015-02-06', '2019-09-29','Environment','Environment protection','O');
INSERT INTO Appel_projet (id_appel_projet, Date_de_lancage, Date_de_fin, Theme, Description, organisme_projet) VALUES ('2', '2015-02-06', '2021-09-29','Food','Food nutrition','O1');
INSERT INTO Appel_projet (id_appel_projet, Date_de_lancage, Date_de_fin, Theme, Description, organisme_projet) VALUES ('3', '2017-02-06', '2023-08-31','Earth','Earth mouvement','O');
INSERT INTO Appel_projet (id_appel_projet, Date_de_lancage, Date_de_fin, Theme, Description, organisme_projet) 
VALUES ('4', '2010-02-06', '2024-10-29','Transport','Transport Design','O1');
INSERT INTO Appel_projet (id_appel_projet, Date_de_lancage, Date_de_fin, Theme, Description, organisme_projet) 
VALUES ('5', '2010-02-06', '2024-10-29','Cinema','Cinema Design','O1');




INSERT INTO Proposition_projet (Id_appel_projet,id_proposition_projet, date_de_depot, date_de_reponse, Reponse, code) VALUES ('1', '1', '2015-03-09', '2015-06-29','refusée','LA');
INSERT INTO Proposition_projet (Id_appel_projet,id_proposition_projet, date_de_depot, date_de_reponse, Reponse, code) VALUES ('2', '2', '2015-04-09', '2015-07-29','refusée','LA');
INSERT INTO Proposition_projet (Id_appel_projet,id_proposition_projet, date_de_depot, date_de_reponse, Reponse, code) VALUES ('3', '3', '2015-05-09', '2015-08-29','acceptée','LB');
INSERT INTO Proposition_projet 
(Id_appel_projet,id_proposition_projet, date_de_depot, date_de_reponse, Reponse, code) 
VALUES ('4', '4', '2015-06-09', '2015-09-29','acceptée','LB');
INSERT INTO Proposition_projet 
(Id_appel_projet,id_proposition_projet, date_de_depot, date_de_reponse, Reponse, code) 
VALUES ('4', '5', '2015-06-09', '2015-09-29','acceptée','LB');
select distinct Id_appel_projet from Proposition_projet;

INSERT INTO Projet (id_projet, Date_de_debut, Date_de_fin, id_proposition_projet,depense ) 
VALUES ('1', '2020-08-14', '2023-09-29','3',
'[
{"id_depense":"1","demandeur":"3","validateur":"4","date_de_depense":"2019-09-11", "montant":"100","type":"fonctionnement"},
{"id_depense":"2","demandeur":"3","validateur":"4","date_de_depense":"2019-05-11","montant":"200","type":"fonctionnement"},
{"id_depense":"3","demandeur":"3","validateur":"4","date_de_depense":"2019-04-11","montant":"20","type":"fonctionnement"}
]');
INSERT INTO Projet (id_projet, Date_de_debut, Date_de_fin, id_proposition_projet,depense ) 
VALUES ('2', '2020-09-05', '2024-10-29','4',
'[
{"id_depense":"1","demandeur":"3","validateur":"4","date_de_depense":"2019-04-11","montant":"20","type":"fonctionnement"}
]');

INSERT INTO Budget (Id_ligne_budget, Id_proposition_projet,Id_projet,Montant, Objet, Type_de_financement) VALUES ('1', '3','1','500','computer','matériel');
INSERT INTO Budget (Id_ligne_budget, Id_proposition_projet,Id_projet,Montant, Objet, Type_de_financement) VALUES ('2', '3','1','400','car','matériel');
INSERT INTO Budget (Id_ligne_budget, Id_proposition_projet,Id_projet,Montant, Objet, Type_de_financement) VALUES ('3', '3','1','200','phone','matériel');

INSERT INTO Budget (Id_ligne_budget, Id_proposition_projet,Id_projet,Montant, Objet, Type_de_financement) VALUES ('4', '4','2','500','computer','matériel');
INSERT INTO Budget (Id_ligne_budget, Id_proposition_projet,Id_projet,Montant, Objet, Type_de_financement) VALUES ('5', '4','2','400','car','matériel');
INSERT INTO Budget (Id_ligne_budget, Id_proposition_projet,Id_projet,Montant, Objet, Type_de_financement) VALUES ('6', '4','2','250','phone','matériel');


INSERT INTO Label (Titre) VALUES ('AOC');
INSERT INTO Label (Titre) VALUES ('IGP');
INSERT INTO Label (Titre) VALUES ('STG');
INSERT INTO Label (Titre) VALUES ('AB');

INSERT INTO Labels_des_propositions_projets (Id_proposition_projet,Titre) VALUES ('1','AOC');
INSERT INTO Labels_des_propositions_projets (Id_proposition_projet,Titre) VALUES ('2','IGP');
INSERT INTO Labels_des_propositions_projets (Id_proposition_projet,Titre) VALUES ('3','STG');
INSERT INTO Labels_des_propositions_projets (Id_proposition_projet,Titre) VALUES ('4','AB');

INSERT INTO Membre_comite_personne_des_organismes_projet (id_membre_comite,organisme_projet) VALUES ('1','O');
INSERT INTO Membre_comite_personne_des_organismes_projet (id_membre_comite,organisme_projet) VALUES ('2','O');
INSERT INTO Membre_comite_personne_des_organismes_projet (id_membre_comite,organisme_projet) VALUES ('3','O1');

INSERT INTO Gerants_des_appels_projets (id_appel_projet,id_membre_comite) VALUES ('1','1');
INSERT INTO Gerants_des_appels_projets (id_appel_projet,id_membre_comite) VALUES ('2','3');
INSERT INTO Gerants_des_appels_projets (id_appel_projet,id_membre_comite) VALUES ('3','2');
INSERT INTO Gerants_des_appels_projets (id_appel_projet,id_membre_comite) VALUES ('4','3');

INSERT INTO Repondeurs_des_propositions_projets (id_proposition_projet,id_membre_comite) VALUES ('1','1');
INSERT INTO Repondeurs_des_propositions_projets (id_proposition_projet,id_membre_comite) VALUES ('2','3');
INSERT INTO Repondeurs_des_propositions_projets (id_proposition_projet,id_membre_comite) VALUES ('3','2');
INSERT INTO Repondeurs_des_propositions_projets (id_proposition_projet,id_membre_comite) VALUES ('4','1');

INSERT INTO Redigeant_des_propositions_projet (id_membre_du_laboratoire,id_proposition_projet) VALUES ('1','1');
INSERT INTO Redigeant_des_propositions_projet (id_membre_du_laboratoire,id_proposition_projet) VALUES ('2','2');
INSERT INTO Redigeant_des_propositions_projet (id_membre_du_laboratoire,id_proposition_projet) VALUES ('3','3');
INSERT INTO Redigeant_des_propositions_projet (id_membre_du_laboratoire,id_proposition_projet) VALUES ('4','4');
INSERT INTO Redigeant_des_propositions_projet (id_membre_du_laboratoire,id_proposition_projet) VALUES ('4','3');

/*INSERT INTO Depense (Id_depense, Id_Projet, Demandeur, Validateur, Date_de_depense, Montant, Type_de_financement) 
VALUES ('1', '1', '3', '4','2019-09-11', '100','fonctionnement');
INSERT INTO Depense (Id_depense, Id_Projet, Demandeur, Validateur, Date_de_depense, Montant, Type_de_financement) 
VALUES ('2', '1', '3', '4','2019-05-11', '200','fonctionnement');
INSERT INTO Depense (Id_depense, Id_Projet, Demandeur, Validateur, Date_de_depense, Montant, Type_de_financement) 
VALUES ('3', '1', '3', '4','2019-04-11', '20','fonctionnement');
INSERT INTO Depense (Id_depense, Id_Projet, Demandeur, Validateur, Date_de_depense, Montant, Type_de_financement) 
VALUES ('4', '1', '3', '4','2019-03-11', '50','fonctionnement');
INSERT INTO Depense (Id_depense, Id_Projet, Demandeur, Validateur, Date_de_depense, Montant, Type_de_financement) 
VALUES ('5', '2', '3', '4','2019-09-11', '50','fonctionnement');*/

INSERT INTO Participant_du_projet (Id_projet,Id_membre_du_laboratoire,Fonction) VALUES ('1','1','Fonction 1');
INSERT INTO Participant_du_projet (Id_projet,Id_membre_du_laboratoire,Fonction) VALUES ('1','2','Fonction 1');
INSERT INTO Participant_du_projet (Id_projet,Id_membre_du_laboratoire,Fonction) VALUES ('1','3','Fonction 2');
INSERT INTO Participant_du_projet (Id_projet,Id_membre_du_laboratoire,Fonction) 
VALUES ('2','2','Fonction 2');
INSERT INTO Participant_du_projet (Id_projet,Id_membre_du_laboratoire,Fonction) 
VALUES ('1','6','Fonction 3');
/*Transformation json en relationel table Projet*/
create view Vjson_relationel_projet as SELECT p.id_projet ,p.Date_de_debut, p.Date_de_fin,p.id_proposition_projet ,
CAST(De->>'id_depense' AS INTEGER) AS id_depense, De->>'demandeur' AS demandeur, 
De->>'validateur' AS validateur,
CAST(De->>'date_de_depense' AS DATE) AS date_de_depense,
CAST(De->>'montant' AS INTEGER) AS montant,
De->>'type' AS type
FROM Projet p, JSON_ARRAY_ELEMENTS(p.depense) De;

/*Somme des dépenses pour chaque projet*/
Create view v_somme_dépenses as 
select  sum(Montant) as Total_depense, Id_projet from Vjson_relationel_projet group by Id_projet;
/*les laboratoires*/
create view vLaboratoire as select entite_juridique.code, entite_juridique.type_entite,entite_juridique.nom,entite_juridique.date_debut_activite,entite_juridique.date_fin_activite 
from laboratoire join entite_juridique on laboratoire.code=entite_juridique.code;
/*Les organismes projets*/
create view  vOrganisme as select entite_juridique.code, entite_juridique.type_entite,
entite_juridique.nom,entite_juridique.date_debut_activite,entite_juridique.date_fin_activite 
from Organisme_projet join entite_juridique on Organisme_projet.code=entite_juridique.code;
/*Les financeurs*/
create view  vFinanceur as select entite_juridique.code, entite_juridique.type_entite,
entite_juridique.nom,entite_juridique.date_debut_activite,entite_juridique.date_fin_activite 
from Financeur join entite_juridique on Financeur.code=entite_juridique.code;



/*
/*Somme des dépenses pour chaque projet*/
--Create view v_somme_dépenses as 
--select  sum(Depense.Montant) as Total_depense, Id_projet from Depense group by Depense.Id_projet; */
/*Somme des budgets pour chaque projet*/
Create view v_somme_budget as select sum(Budget.Montant) as Total_budget, Id_projet from Budget group by Budget.Id_projet;
/*Projets auquels restent budget*/
Create view v_projet_auquel_reste_budget as 
select v_somme_dépenses.Id_projet ,v_somme_dépenses.Total_depense, v_somme_budget.Total_budget,  v_somme_budget.Total_budget-v_somme_dépenses.Total_depense as différence 
from v_somme_dépenses join v_somme_budget on v_somme_dépenses.Id_projet=v_somme_budget.Id_projet 
where v_somme_budget.Total_budget-v_somme_dépenses.Total_depense>0;
select * from v_projet_auquel_reste_budget;

/*Projets en cours*/
Create view v_projet_en_cours as select * from Projet where Date_de_fin>CURRENT_DATE;
/*Appel projet en cours*/
create view v_appel_projet_en_cours as select * from Appel_projet where Date_de_fin>CURRENT_DATE;
/*Appel projet avec réponse*/
create view v_appel_projet_avec_reponse as select distinct Id_appel_projet from Proposition_projet;
/*Appel projet en cours avec réponse*/
create view v_appel_projet_en_cours_avec_reponse as select v_appel_projet_en_cours.id_appel_projet,v_appel_projet_en_cours.Date_de_lancage,v_appel_projet_en_cours.Date_de_fin,v_appel_projet_en_cours.Theme,v_appel_projet_en_cours.Description,v_appel_projet_en_cours.organisme_projet 
from v_appel_projet_en_cours 
join v_appel_projet_avec_reponse on v_appel_projet_en_cours.Id_appel_projet=v_appel_projet_avec_reponse.Id_appel_projet;
/*Proposition_projet_accepté*/
create view v_proposition_projet_accepté as select * from Proposition_projet where reponse='acceptée';
/*propositions de projets des projets en cours*/ 
create view  proposition_projet_en_cours as select id_projet,v_proposition_projet_accepté.id_proposition_projet,date_de_depot,date_de_reponse,reponse,code from v_projet_en_cours  join v_proposition_projet_accepté 
on v_projet_en_cours.id_proposition_projet = v_proposition_projet_accepté.id_proposition_projet;

/*Les laboratoires des membres qui participent aux projets*/
create view v_Les_laboratoires_des_membres_qui_participe_aux_projets as select distinct  Participant_du_projet.id_projet,Code,Membre_du_laboratoire.id_membre_du_laboratoire,type_entite, Entite_juridique.Nom, date_debut_activite from Participant_du_projet join Membre_du_laboratoire 
on Participant_du_projet.Id_membre_du_laboratoire=Membre_du_laboratoire.id_membre_du_laboratoire join Entite_juridique 
on Entite_juridique.code=laboratoire;

/*les membres de laboratoire qui ont rédigé les propositions de projets */
create view v_Les_membres_du_laboratoire_qui_redige_propositions_projets as
select Membre_du_laboratoire.id_membre_du_laboratoire,Membre_du_laboratoire.nom,Membre_du_laboratoire.type from Redigeant_des_propositions_projet join Membre_du_laboratoire 
on Redigeant_des_propositions_projet.id_membre_du_laboratoire=Membre_du_laboratoire.id_membre_du_laboratoire;

/*les membres de laboratoire qui ont répondu aux propositions de projets */
create view v_Les_membres_du_laboratoire_qui_repond_propositions_projets as select Membre_comite_personne.id_membre_comite,nom,id_proposition_projet from Repondeurs_des_propositions_projets join Membre_comite_personne 
on Repondeurs_des_propositions_projets.id_membre_comite=Membre_comite_personne.id_membre_comite;

/*nombre de proposition de projets acceptées par type d'organisme*/
create view v_nb_proposition_projet_par_type_organisme as select count(id_appel_projet), type_entite from Proposition_projet join Entite_juridique 
on Proposition_projet.code = Entite_juridique.code group by type_entite;
/*nombre de personnes associées à chaque projet*/
create view v_nb_personne_associe_projet as select id_projet, count(Id_membre_du_laboratoire) as nombre_personne 
from Participant_du_projet group by id_projet;

/* somme des dépenses de chaque mois pour chaque projet*/
create view v_somme_depense_mensuel_projet 
as select sum(montant) as somme_depense , EXTRACT(month from Date_de_depense) as Mois, id_projet 
from Vjson_relationel_projet group by montant,Date_de_depense,id_projet;

/* Le mois ou y'a plus de dépenses pour chaque projet*/
create view test as select distinct max(somme_depense) as somme_depense,id_projet 
from v_somme_depense_mensuel_projet group by id_projet;
create view v_mois_max_depense_pour_projet as select v_somme_depense_mensuel_projet.id_projet,v_somme_depense_mensuel_projet.somme_depense,v_somme_depense_mensuel_projet.mois from test  left join v_somme_depense_mensuel_projet 
on v_somme_depense_mensuel_projet.somme_depense=test.somme_depense and v_somme_depense_mensuel_projet.id_projet=test.id_projet;



/*Nombre de projet affectés à chaque membre du laboratoire*/
create view v_nb_projet_associe_a_chaque_membre_laboratoire as 
select  Participant_du_projet.Id_membre_du_laboratoire, Membre_du_laboratoire.nom ,count(id_projet) as Nombre_projets from Participant_du_projet 
join Membre_du_laboratoire on Participant_du_projet.Id_membre_du_laboratoire=Membre_du_laboratoire.id_membre_du_laboratoire 
group by Participant_du_projet.Id_membre_du_laboratoire, Membre_du_laboratoire.nom ;

create view v_nombre_projet_maximum_associe_a_un_membre as 
select max(nombre_projets) as nombre_projets from v_nb_projet_associe_a_chaque_membre_laboratoire;
/*membres des laboratoire affectés au plus grand nombre de projet en cours*/
create view v_membre_laboratoire_affecte_max_nb_projet as select B.id_membre_du_laboratoire, nom, B.nombre_projets from v_nombre_projet_maximum_associe_a_un_membre as A left join v_nb_projet_associe_a_chaque_membre_laboratoire as B 
on A.nombre_projets=B.nombre_projets;






/*Membre du laboratoire associé à plus de projet*/
select max(nombre_projets) as Nombre_projet_maximum
from v_nb_projet_associe_a_chaque_membre_laboratoire ;
/* les appels d'offre en cours non répondus*/
create view v_appel_offre_en_cours_non_repondu as 
select v_appel_projet_en_cours.id_appel_projet,v_appel_projet_en_cours.date_de_lancage,v_appel_projet_en_cours.date_de_fin,v_appel_projet_en_cours.theme,v_appel_projet_en_cours.description,v_appel_projet_en_cours.organisme_projet 
from v_appel_projet_en_cours left join Proposition_projet on Proposition_projet.Id_appel_projet=v_appel_projet_en_cours.Id_appel_projet
where Proposition_projet.Id_appel_projet is null;
/*Les enseigants chercheurs */
create view vEnseignant_chercheur as select id_membre_du_laboratoire,nom,Qte_temps_recherche, etablissement_associe from Membre_du_laboratoire where type = 'Enseignant_chercheur';
/*Les ingenieurs de recherche*/
create view vIngenieur_de_recherche as select id_membre_du_laboratoire,nom,domaine_specialite from Membre_du_laboratoire where type = 'Ingenieur_de_recherche';
/*Les doctorants*/
create view vDoctorant as select id_membre_du_laboratoire,nom, date_debut_these, date_fin_these, sujet_these from Membre_du_laboratoire where type = 'Doctorant';



--- Les membres du laboratoire peut lire, ajouter, modifier et supprimer des données sur les tables laboratoire, aux propositions de projets, 
--aux employés du laboratoire, aux participants du projet, aux dépenses et aux budgets du projet. Cela lui permettra d'avoir une vue d'ensemble sur son laboratoire lui facilitant la prise de décision afin de mieux gérer ces projets.
/*CREATE USER membre_du_laboratoire;
GRANT SELECT, INSERT, DELETE, UPDATE
ON Membre_du_laboratoire,Financeur,Organisme_projet,Entite_juridique,Laboratoire
,Proposition_projet,Projet,Budget,Participant_du_projet,Depense
TO membre_du_laboratoire;

CREATE USER Membre_des_organsimes_projets;
GRANT SELECT, INSERT, DELETE, UPDATE
ON Financeur,Organisme_projet,Entite_juridique, 
Appel_projet,Gerants_des_appels_projets,Repondeurs_des_propositions_projets
TO Membre_des_organsimes_projets;

-- Créer un utilisateur avec des super droits: superadmin (il peut accéder à toutes les tables de la bdd)
CREATE ROLE superpadmin WITH SUPERUSER LOGIN PASSWORD 'superadmin';

GRANT SELECT, INSERT, DELETE, UPDATE
ON Membre_du_laboratoire,Financeur,Organisme_projet,Entite_juridique,Laboratoire
,Proposition_projet,Projet,Budget,Participant_du_projet, Financeur,Organisme_projet,Entite_juridique, 
Appel_projet,Gerants_des_appels_projets,Repondeurs_des_propositions_projets, Label, Contact,Depense
TO superadmin;

GRANT SELECT
ON v_somme_dépenses,v_projet_en_cours,v_projet_auquel_reste_budget,v_nombre_projet_maximum_associe_a_un_membre,
v_nb_projet_associe_a_chaque_membre_laboratoire,v_nb_personne_associe_projet,v_mois_max_depense_pour_projet
,v_membre_laboratoire_affecte_max_nb_projet,vjson_relationel_projet, v_somme_depense_mensuel_projet, v_somme_budget, v_les_laboratoires_des_membres_qui_participe_aux_projets,vEnseignant_chercheur,vIngenieur_de_recherche,vDoctorant
TO membre_du_laboratoire;

GRANT SELECT on 
v_proposition_projet_accepté,v_nb_proposition_projet_par_type_organisme,v_les_membres_du_laboratoire_qui_repond_propositions_projets
,v_les_membres_du_laboratoire_qui_redige_propositions_projets
,v_appel_projet_en_cours_avec_reponse,v_appel_projet_en_cours,v_appel_projet_avec_reponse,v_appel_offre_en_cours_non_repondu
TO membre_du_laboratoire,Membre_des_organsimes_projets; */




